package com.co.pragma.practicas.java.microservicios.servicioarchivos;

import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

public class DatoTest {
    public static FotoCliente cliente1() throws IOException {
        var resource = new ClassPathResource("imagen1.jpg");
        return new FotoCliente(1, resource.getInputStream().readAllBytes());
    }

    public static FotoCliente cliente2() throws IOException {
        var resource = new ClassPathResource("imagen2.jpg");
        return new FotoCliente(2, resource.getInputStream().readAllBytes());
    }

    public static FotoCliente cliente3() throws IOException {
        var resource = new ClassPathResource("imagen3.jpg");
        return new FotoCliente(3, resource.getInputStream().readAllBytes());
    }

    public static FotoCliente cliente5() throws IOException {
        var resource = new ClassPathResource("imagen3.jpg");
        return new FotoCliente(5, resource.getInputStream().readAllBytes());
    }
}
