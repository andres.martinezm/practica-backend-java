package com.co.pragma.practicas.java.microservicios.servicioarchivos.services;

import com.co.pragma.practicas.java.microservicios.servicioarchivos.DatoTest;
import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class FotoClienteServiceTest {

    @Autowired
    private FotoClienteService fotoClienteService;

    @BeforeAll
    static void setUp(@Autowired FotoClienteService fotoClienteService) throws IOException {
        Arrays.asList(DatoTest.cliente1(), DatoTest.cliente2(), DatoTest.cliente3(), DatoTest.cliente5()).forEach(fotoClienteService::save);
    }

    @Test
    void findAll() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteService.findAll();
        assertFalse(clientes.isEmpty());
        assertTrue(clientes.stream().anyMatch(c -> c.getCliente().equals(2)));
    }

    @Test
    void testFindAll() {
        assertThrows(UnsupportedOperationException.class, () -> fotoClienteService.findAll(PageRequest.of(0, 10)));
    }

    @Test
    void findById() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteService.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);

        assertTrue(fotoClienteService.findById(c.getId()).isPresent());
    }

    @Test
    void save() throws IOException {
        var resource = new ClassPathResource("imagen2.jpg");
        FotoCliente cliente4 = fotoClienteService.save(new FotoCliente(4, resource.getInputStream().readAllBytes()));
        assertAll(() -> assertNotNull(cliente4),
                () -> assertNotNull(cliente4.getId()),
                () -> assertNotNull(cliente4.getCliente()),
                () -> assertNotNull(cliente4.getFoto()));
    }

    @Test
    void update() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteService.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);
        c.setCliente(33);
        fotoClienteService.save(c);

        Optional<FotoCliente> cl = fotoClienteService.findById(c.getId());
        assertTrue(cl.isPresent());
        assertEquals(33, cl.get().getCliente());
    }

    @Test
    void delete() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteService.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);
        fotoClienteService.delete(c);

        assertTrue(fotoClienteService.findById(c.getId()).isEmpty());
    }

    @Test
    void buscarPorClienteId() {
        assertTrue(fotoClienteService.buscarPorClienteId(3).isPresent());
        assertTrue(fotoClienteService.buscarPorClienteId(44).isEmpty());
    }

    @Test
    void existeFotoPorClienteId() {
        assertTrue(fotoClienteService.existeFotoPorClienteId(5));
        assertFalse(fotoClienteService.existeFotoPorClienteId(44));
    }

    @Test
    void eliminarPorClienteId() {
        fotoClienteService.eliminarPorClienteId(3);
        assertTrue(fotoClienteService.buscarPorClienteId(3).isEmpty());
    }
}