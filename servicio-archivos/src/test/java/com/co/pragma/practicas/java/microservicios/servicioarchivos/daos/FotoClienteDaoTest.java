package com.co.pragma.practicas.java.microservicios.servicioarchivos.daos;

import com.co.pragma.practicas.java.microservicios.servicioarchivos.DatoTest;
import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
class FotoClienteDaoTest {

    @Autowired
    private FotoClienteDao fotoClienteDao;

    @BeforeEach
    void setUp() throws IOException {
        fotoClienteDao.saveAll(Arrays.asList(DatoTest.cliente1(), DatoTest.cliente2(), DatoTest.cliente3()));
    }

    @Test
    void save() throws IOException {
        var resource = new ClassPathResource("imagen2.jpg");
        FotoCliente cliente4 = fotoClienteDao.save(new FotoCliente(4, resource.getInputStream().readAllBytes()));
        assertAll(() -> assertNotNull(cliente4),
                () -> assertNotNull(cliente4.getId()),
                () -> assertNotNull(cliente4.getCliente()),
                () -> assertNotNull(cliente4.getFoto()));
    }

    @Test
    void findAll() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteDao.findAll();
        assertFalse(clientes.isEmpty());
        assertTrue(clientes.stream().anyMatch(c -> c.getCliente().equals(2)));
    }

    @Test
    void findById() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteDao.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);

        assertTrue(fotoClienteDao.findById(c.getId()).isPresent());
    }

    @Test
    void update() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteDao.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);
        c.setCliente(33);
        fotoClienteDao.save(c);

        Optional<FotoCliente> cl = fotoClienteDao.findById(c.getId());
        assertTrue(cl.isPresent());
        assertEquals(33, cl.get().getCliente());
    }

    @Test
    void delete() {
        List<FotoCliente> clientes = (List<FotoCliente>) fotoClienteDao.findAll();
        assertFalse(clientes.isEmpty());
        FotoCliente c = clientes.get(0);
        fotoClienteDao.delete(c);

        assertTrue(fotoClienteDao.findById(c.getId()).isEmpty());
    }

    @Test
    void findByCliente() {
        assertTrue(fotoClienteDao.findByCliente(2).isPresent());
        assertTrue(fotoClienteDao.findByCliente(44).isEmpty());
    }

    @Test
    void existsByCliente() {
        assertTrue(fotoClienteDao.existsByCliente(3));
        assertFalse(fotoClienteDao.existsByCliente(33));
    }

    @Test
    void deleteByCliente() {
        fotoClienteDao.deleteByCliente(3);
        assertTrue(fotoClienteDao.findByCliente(3).isEmpty());
    }
}