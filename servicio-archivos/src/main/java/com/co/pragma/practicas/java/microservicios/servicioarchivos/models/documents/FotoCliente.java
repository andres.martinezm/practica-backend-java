package com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("fotos_cliente")
public class FotoCliente {

    @Id
    private String id;

    private Integer cliente;
    private byte[] foto;

    public FotoCliente() {
    }

    public FotoCliente(Integer cliente, byte[] foto) {
        this.cliente = cliente;
        this.foto = foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }
}
