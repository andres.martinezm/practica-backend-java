package com.co.pragma.practicas.java.microservicios.servicioarchivos.services;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.services.MasterService;
import com.co.pragma.practicas.java.microservicios.servicioarchivos.daos.FotoClienteDao;
import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class FotoClienteService extends MasterService<FotoCliente, String, FotoClienteDao> {

    public FotoClienteService(FotoClienteDao repository) {
        super(repository);
    }

    @Transactional(readOnly = true)
    public Optional<FotoCliente> buscarPorClienteId(int clienteId) {
        return dao.findByCliente(clienteId);
    }

    @Transactional(readOnly = true)
    public boolean existeFotoPorClienteId(int clienteId) {
        return dao.existsByCliente(clienteId);
    }

    public void eliminarPorClienteId(int clienteId) {
        dao.deleteByCliente(clienteId);
    }
}
