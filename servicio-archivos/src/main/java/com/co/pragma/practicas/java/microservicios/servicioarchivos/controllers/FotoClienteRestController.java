package com.co.pragma.practicas.java.microservicios.servicioarchivos.controllers;

import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import com.co.pragma.practicas.java.microservicios.servicioarchivos.services.FotoClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/fotos-clientes")
@Api(tags = "Fotos Clientes")
public class FotoClienteRestController {

    private final FotoClienteService fotoClienteService;
    private final Map<String, String> MSG_ERROR_ARCHIVO_VACIO = Map.of("error", "Debe subir el archivo");

    public FotoClienteRestController(FotoClienteService fotoClienteService) {
        this.fotoClienteService = fotoClienteService;
    }

    @ApiOperation("Consultar todos las fotos de los clientes")
    @ApiResponse(code = 200, message = "Ok", response = FotoCliente.class, responseContainer = "List")
    @GetMapping
    public ResponseEntity<Iterable<FotoCliente>> consultarTodos() {
        return ResponseEntity.ok(fotoClienteService.findAll());
    }

    @ApiOperation("Buscar foto cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = FotoCliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @GetMapping("/{id}")
    public ResponseEntity<FotoCliente> buscarPorId(@PathVariable String id) {
        return fotoClienteService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @ApiOperation("Buscar foto cliente por identificador del cliente")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = FotoCliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @GetMapping("/cliente/{id}")
    public ResponseEntity<FotoCliente> buscarPorClienteId(@PathVariable Integer id) {
        return fotoClienteService.buscarPorClienteId(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @ApiOperation("Eliminar foto cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Eliminado", response = FotoCliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable String id) {
        Optional<FotoCliente> optFotoCl = fotoClienteService.findById(id);
        if (optFotoCl.isPresent()) {
            fotoClienteService.delete(optFotoCl.get());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Eliminar foto cliente por identificador del cliente")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Eliminado", response = FotoCliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @DeleteMapping("/cliente/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable Integer id) {
        if (fotoClienteService.existeFotoPorClienteId(id)) {
            fotoClienteService.eliminarPorClienteId(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Registra una foto de cliente")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Creado", response = FotoCliente.class),
            @ApiResponse(code = 400, message = "Debe subir el archivo")
    })
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> registrar(@RequestParam Integer clienteId, @RequestParam MultipartFile archivo) throws IOException {
        if (archivo.isEmpty())
            return ResponseEntity.badRequest().body(MSG_ERROR_ARCHIVO_VACIO);
        var fotoCl = new FotoCliente(clienteId, archivo.getBytes());
        fotoCl = fotoClienteService.save(fotoCl);
        return ResponseEntity.status(HttpStatus.CREATED).body(fotoCl);
    }

    @ApiOperation("Actualiza la foto de un cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Actualizado", response = FotoCliente.class),
            @ApiResponse(code = 400, message = "Debe subir el archivo"),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @PutMapping(path = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> actualizar(@PathVariable String id, @RequestParam MultipartFile archivo) throws IOException {
        if (archivo.isEmpty())
            return ResponseEntity.badRequest().body(MSG_ERROR_ARCHIVO_VACIO);
        Optional<FotoCliente> optFotoCl = fotoClienteService.findById(id);
        if (optFotoCl.isPresent()) {
            optFotoCl.get().setFoto(archivo.getBytes());
            fotoClienteService.save(optFotoCl.get());
            return ResponseEntity.status(HttpStatus.CREATED).body(optFotoCl.get());
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Actualiza la foto de un cliente por identificador del cliente")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Actualizado", response = FotoCliente.class),
            @ApiResponse(code = 400, message = "Debe subir el archivo"),
            @ApiResponse(code = 404, message = "No se encuentra el registro")
    })
    @PutMapping(path = "/cliente/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> actualizar(@PathVariable Integer id, @RequestParam MultipartFile archivo) throws IOException {
        if (archivo.isEmpty())
            return ResponseEntity.badRequest().body(MSG_ERROR_ARCHIVO_VACIO);
        Optional<FotoCliente> optFotoCl = fotoClienteService.buscarPorClienteId(id);
        if (optFotoCl.isPresent()) {
            optFotoCl.get().setFoto(archivo.getBytes());
            fotoClienteService.save(optFotoCl.get());
            return ResponseEntity.status(HttpStatus.CREATED).body(optFotoCl.get());
        }
        return ResponseEntity.notFound().build();
    }
}
