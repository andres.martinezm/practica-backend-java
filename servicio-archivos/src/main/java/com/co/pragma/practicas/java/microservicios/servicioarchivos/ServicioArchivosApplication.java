package com.co.pragma.practicas.java.microservicios.servicioarchivos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioArchivosApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicioArchivosApplication.class, args);
    }

}
