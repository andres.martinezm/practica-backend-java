package com.co.pragma.practicas.java.microservicios.servicioarchivos.daos;

import com.co.pragma.practicas.java.microservicios.servicioarchivos.models.documents.FotoCliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FotoClienteDao extends CrudRepository<FotoCliente, String> {
    Optional<FotoCliente> findByCliente(int clienteId);

    boolean existsByCliente(int clienteId);

    void deleteByCliente(int clienteId);
}
