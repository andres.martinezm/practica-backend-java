package com.co.pragma.practicas.java.microservicios.servicioparametros.controllers;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class TipoIdentificacionRestControllerTest {

    private static ObjectMapper mapper;
    private final String ENDPOINT_BASE = "/tipos-identificacion";
    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void beforeAll() {
        mapper = new ObjectMapper();
    }

    @Test
    void registrar() throws Exception {
        TipoIdentificacion ti = new TipoIdentificacion(null, "PRUEBA", "PR");
        mvc.perform(post(ENDPOINT_BASE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(ti)))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.tipoIdentificacion").value(5));
    }

    @Test
    void consultarTodos() throws Exception {
        mvc.perform(get(ENDPOINT_BASE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[?(@.descripcion == 'PASAPORTE')]", hasSize(1)));
    }

    @Test
    void buscarPorId() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.descripcion").value("CEDULA DE CIUDADANIA"));
    }

    @Test
    void actualizar() throws Exception {
        String jsonBody = mvc.perform(get(ENDPOINT_BASE + "/2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.tipoIdentificacion").value(2))
                .andReturn().getResponse().getContentAsString(); // No sé si hay alguna forma mejor o más directa de obtener el objeto
        TipoIdentificacion ti = mapper.readValue(jsonBody, TipoIdentificacion.class);
        ti.setDescripcion("PRUEBA");
        ti.setCodificacion("TI");

        mvc.perform(put(ENDPOINT_BASE + "/" + ti.getTipoIdentificacion())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(ti)))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[?(@.descripcion == 'PRUEBA')]", hasSize(1)));
    }

    @Test
    void eliminar() throws Exception {
        mvc.perform(delete(ENDPOINT_BASE + "/3"))
                .andExpect(status().is(HttpStatus.OK.value()));
        mvc.perform(get(ENDPOINT_BASE + "/3"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }
}