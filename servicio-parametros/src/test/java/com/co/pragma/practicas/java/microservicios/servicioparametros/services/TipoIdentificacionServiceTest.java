package com.co.pragma.practicas.java.microservicios.servicioparametros.services;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TipoIdentificacionServiceTest {

    @Autowired
    private TipoIdentificacionService tipoIdentificacionService;

    @Test
    void findAll() {
        List<TipoIdentificacion> lista = (List<TipoIdentificacion>) tipoIdentificacionService.findAll();
        assertFalse(lista.isEmpty());
    }

    @Test
    void findAllPage() {
        assertThrows(UnsupportedOperationException.class, () -> tipoIdentificacionService.findAll(PageRequest.of(0, 20)));
    }

    @Test
    void findById() {
        Optional<TipoIdentificacion> optTipoId = tipoIdentificacionService.findById(4);
        assertTrue(optTipoId.isPresent());
        assertEquals("PASAPORTE", optTipoId.get().getDescripcion());
    }

    @Test
    void save() {
        TipoIdentificacion entity = tipoIdentificacionService.save(new TipoIdentificacion(null, "PRUEBA", "PR"));
        assertNotNull(entity.getTipoIdentificacion());
        assertEquals(5, entity.getTipoIdentificacion());
    }

    @Test
    void update() {
        Optional<TipoIdentificacion> optTipoId = tipoIdentificacionService.findById(2);
        assertTrue(optTipoId.isPresent());
        TipoIdentificacion tipoIdentificacion = optTipoId.get();
        tipoIdentificacion.setDescripcion("PRUEBA");
        tipoIdentificacion.setCodificacion("PR");
        tipoIdentificacion = tipoIdentificacionService.save(tipoIdentificacion);
        assertNotNull(tipoIdentificacion);
        assertEquals("PRUEBA", tipoIdentificacion.getDescripcion());
        assertEquals("PR", tipoIdentificacion.getCodificacion());
    }

    @Test
    void delete() {
        Optional<TipoIdentificacion> optTipoId = tipoIdentificacionService.findById(3);
        assertTrue(optTipoId.isPresent());
        tipoIdentificacionService.delete(optTipoId.get());
        optTipoId = tipoIdentificacionService.findById(3);
        assertTrue(optTipoId.isEmpty());
    }
}