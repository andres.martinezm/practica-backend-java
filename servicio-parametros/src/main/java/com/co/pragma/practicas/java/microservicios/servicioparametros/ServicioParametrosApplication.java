package com.co.pragma.practicas.java.microservicios.servicioparametros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.co.pragma.practicas.java.microservicios")
public class ServicioParametrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioParametrosApplication.class, args);
	}

}
