package com.co.pragma.practicas.java.microservicios.servicioparametros.controllers;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.co.pragma.practicas.java.microservicios.servicioparametros.models.dtos.TipoIdentificacionDto;
import com.co.pragma.practicas.java.microservicios.servicioparametros.services.TipoIdentificacionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/tipos-identificacion")
public class TipoIdentificacionRestController {

    private final TipoIdentificacionService tipoIdentificacionService;

    public TipoIdentificacionRestController(TipoIdentificacionService tipoIdentificacionService) {
        this.tipoIdentificacionService = tipoIdentificacionService;
    }

    @ApiOperation("Registra el tipo de identificación y devuelve el registro del tipo de identificación")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Creado", response = TipoIdentificacion.class),
            @ApiResponse(code = 422, message = "No se pudo registrar el tipo de identificación, modifique la entidad e intente de nuevo")
    })
    @PostMapping
    public ResponseEntity<TipoIdentificacion> registrar(@RequestBody TipoIdentificacionDto tipoIdentificacionDto) {
        TipoIdentificacion ti = new TipoIdentificacion();
        ti.setDescripcion(tipoIdentificacionDto.getDescripcion());
        ti.setCodificacion(tipoIdentificacionDto.getCodificacion());
        ti = tipoIdentificacionService.save(ti);
        return ti.getTipoIdentificacion() != null ? ResponseEntity.status(HttpStatus.CREATED).body(ti) :
                ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
    }

    @ApiOperation("Consulta todos los clientes y devuelve un listado de clientes")
    @ApiResponse(code = 200, message = "Ok", response = TipoIdentificacion.class, responseContainer = "List")
    @GetMapping
    public ResponseEntity<Iterable<TipoIdentificacion>> consultarTodos() {
        return ResponseEntity.ok(tipoIdentificacionService.findAll());
    }

    @ApiOperation("Consulta el tipo de identificación por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = TipoIdentificacion.class),
            @ApiResponse(code = 404, message = "No se encuentra el tipo de identificación")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TipoIdentificacion> buscarPorId(@PathVariable int id) {
        return tipoIdentificacionService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @ApiOperation("Actualiza el tipo de identificación por identificador")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Actualizado", response = TipoIdentificacion.class),
            @ApiResponse(code = 404, message = "No se encuentra el tipo de identificación")
    })
    @PutMapping("/{id}")
    public ResponseEntity<TipoIdentificacion> actualizar(@PathVariable int id, @RequestBody TipoIdentificacionDto tipoIdentificacionDto) {
        Optional<TipoIdentificacion> optTipoId = tipoIdentificacionService.findById(id);
        if (optTipoId.isPresent()) {
            TipoIdentificacion ti = optTipoId.get();
            ti.setCodificacion(tipoIdentificacionDto.getCodificacion());
            ti.setDescripcion(tipoIdentificacionDto.getDescripcion());
            tipoIdentificacionService.save(ti);
            return ResponseEntity.status(HttpStatus.CREATED).body(ti);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Elimina el tipo de identificación por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Eliminado"),
            @ApiResponse(code = 404, message = "No se encuentra el tipo de identificación")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable int id) {
        Optional<TipoIdentificacion> optTipoId = tipoIdentificacionService.findById(id);
        if (optTipoId.isPresent()) {
            tipoIdentificacionService.delete(optTipoId.get());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
