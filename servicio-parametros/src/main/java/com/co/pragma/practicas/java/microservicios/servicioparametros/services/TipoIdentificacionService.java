package com.co.pragma.practicas.java.microservicios.servicioparametros.services;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.co.pragma.practicas.java.microservicios.libs.libcommon.services.MasterService;
import com.co.pragma.practicas.java.microservicios.servicioparametros.daos.TipoIdentificacionDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TipoIdentificacionService extends MasterService<TipoIdentificacion, Integer, TipoIdentificacionDao> {

    public TipoIdentificacionService(TipoIdentificacionDao repository) {
        super(repository);
    }
}
