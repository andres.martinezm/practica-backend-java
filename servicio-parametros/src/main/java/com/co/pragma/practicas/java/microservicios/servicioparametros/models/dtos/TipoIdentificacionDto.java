package com.co.pragma.practicas.java.microservicios.servicioparametros.models.dtos;

public class TipoIdentificacionDto {
    private String descripcion;
    private String codificacion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }
}
