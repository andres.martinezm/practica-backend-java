package com.co.pragma.practicas.java.microservicios.servicioparametros.daos;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import org.springframework.data.repository.CrudRepository;

public interface TipoIdentificacionDao extends CrudRepository<TipoIdentificacion, Integer> {
}
