package com.co.pragma.practicas.java.microservicios.servicioclientes.controllers;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.ClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import com.co.pragma.practicas.java.microservicios.servicioclientes.services.ClienteService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("/clientes")
@Api(tags = "Clientes")
public class ClienteRestController {

    private final ClienteService clienteService;
    private final Map<String, String> MSG_ERROR_ARCHIVO_VACIO = Map.of("error", "Debe subir el archivo");
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public ClienteRestController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @ApiOperation("Registra un cliente sin foto y devuelve el registro del cliente")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Creado", response = Cliente.class),
            @ApiResponse(code = 422, message = "No se pudo registrar el cliente, modifique la entidad e intente de nuevo")
    })
    @PostMapping
    public ResponseEntity<Cliente> registrar(@RequestBody ClienteDto clienteDto) {
        Cliente cliente = new Cliente(clienteDto.getNombres(), clienteDto.getApellidos(), new TipoIdentificacion(clienteDto.getTipoIdentificacion(), null, null),
                clienteDto.getIdentificacion(), clienteDto.getEdad(), clienteDto.getCiudadNacimiento());
        cliente = clienteService.save(cliente);
        return cliente.getCliente() != null ? ResponseEntity.status(HttpStatus.CREATED).body(cliente) :
                ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
    }

    @ApiOperation("Registra un cliente con foto y devuelve el registro del cliente")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Creado", response = FotoClienteDto.class),
            @ApiResponse(code = 204, message = "No se pudo realizar el registro"),
            @ApiResponse(code = 422, message = "No se pudo registrar el cliente, modifique la entidad e intente de nuevo"),
            @ApiResponse(code = 400, message = "Debe subir el archivo")
    })
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> registrar(@ApiParam("Cadena de texto en formato JSON de la clase ClienteDto") @RequestParam String clienteJson, @RequestParam MultipartFile archivo) {
        if (archivo.isEmpty())
            return ResponseEntity.badRequest().body(MSG_ERROR_ARCHIVO_VACIO);
        ObjectMapper mapper = new ObjectMapper();
        try {
            FotoClienteDto dto = clienteService.registrarConFoto(mapper.readValue(clienteJson, ClienteDto.class), archivo);
            return dto != null ? ResponseEntity.status(HttpStatus.CREATED).body(dto) :
                    ResponseEntity.noContent().build();
        } catch (JsonProcessingException e) {
            logger.severe(e.getMessage());
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
    }

    @ApiOperation("Consulta todos los clientes y devuelve un listado de clientes")
    @ApiResponse(code = 200, message = "Ok", response = Cliente.class, responseContainer = "List")
    @GetMapping
    public ResponseEntity<Iterable<Cliente>> consultarTodos() {
        return ResponseEntity.ok(clienteService.findAll());
    }

    @ApiOperation("Consulta todos los clientes con su foto y devuelve un listado de clientes")
    @ApiResponse(code = 200, message = "Ok", response = FotoClienteDto.class, responseContainer = "List")
    @GetMapping("/fotos")
    public ResponseEntity<Iterable<FotoClienteDto>> consultarTodosConFoto() {
        return ResponseEntity.ok(clienteService.consultarTodosConFoto());
    }

    @ApiOperation("Consulta todos los clientes por paginación y devuelve un listado de cliente con información de la paginación")
    @ApiResponse(code = 200, message = "Ok", response = Cliente.class, responseContainer = "List")
    @GetMapping("/paginar")
    public ResponseEntity<Page<Cliente>> consultarTodos(Pageable paginacion) {
        return ResponseEntity.ok(clienteService.findAll(paginacion));
    }

    @ApiOperation("Consulta todos los clientes con su foto por paginación y devuelve un listado de cliente con información de la paginación")
    @ApiResponse(code = 200, message = "Ok", response = FotoClienteDto.class, responseContainer = "List")
    @GetMapping("/paginar/fotos")
    public ResponseEntity<Page<FotoClienteDto>> consultarTodosConFoto(Pageable paginacion) {
        return ResponseEntity.ok(clienteService.consultarTodosConFoto(paginacion));
    }

    @ApiOperation("Consulta el cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Cliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el cliente")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> buscarPorId(@PathVariable int id) {
        return clienteService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @ApiOperation("Consulta el cliente con su foto por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = FotoClienteDto.class),
            @ApiResponse(code = 404, message = "No se encuentra el cliente")
    })
    @GetMapping("/{id}/foto")
    public ResponseEntity<FotoClienteDto> buscarPorIdConFoto(@PathVariable int id) {
        return clienteService.buscarPorIdConFoto(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @ApiOperation("Actualiza el cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Actualizado", response = Cliente.class),
            @ApiResponse(code = 404, message = "No se encuentra el cliente")
    })
    @PutMapping("/{id}")
    public ResponseEntity<Cliente> actualizar(@PathVariable int id, @RequestBody ClienteDto clienteDto) {
        Optional<Cliente> optCliente = clienteService.findById(id);
        if (optCliente.isPresent()) {
            Cliente cliente = optCliente.get();
            cliente.setNombres(clienteDto.getNombres());
            cliente.setApellidos(clienteDto.getApellidos());
            cliente.setEdad(clienteDto.getEdad());
            cliente.setCiudadNacimiento(clienteDto.getCiudadNacimiento());
            cliente.setIdentificacion(clienteDto.getIdentificacion());
            TipoIdentificacion ti = new TipoIdentificacion();
            ti.setTipoIdentificacion(clienteDto.getTipoIdentificacion());
            cliente.setTipoIdentificacion(ti);
            clienteService.save(cliente);
            return ResponseEntity.status(HttpStatus.CREATED).body(cliente);
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Actualiza el cliente con foto por identificador")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Actualizado", response = FotoClienteDto.class),
            @ApiResponse(code = 204, message = "No se pudo realizar la actualización"),
            @ApiResponse(code = 422, message = "No se pudo actualizar el cliente, modifique la entidad e intente de nuevo"),
            @ApiResponse(code = 400, message = "Debe subir el archivo"),
            @ApiResponse(code = 404, message = "No se encuentra el cliente")
    })
    @PutMapping(path = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> actualizar(@PathVariable int id, @ApiParam("Cadena de texto en formato JSON de la clase ClienteDto") @RequestParam String clienteJson,
                                             @RequestParam MultipartFile archivo) {
        if (archivo.isEmpty())
            return ResponseEntity.badRequest().body(MSG_ERROR_ARCHIVO_VACIO);
        Optional<Cliente> optCliente = clienteService.findById(id);
        if (optCliente.isPresent()) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                ClienteDto clienteDto = mapper.readValue(clienteJson, ClienteDto.class);
                Cliente cliente = optCliente.get();
                cliente.setNombres(clienteDto.getNombres());
                cliente.setApellidos(clienteDto.getApellidos());
                cliente.setEdad(clienteDto.getEdad());
                cliente.setCiudadNacimiento(clienteDto.getCiudadNacimiento());
                cliente.setIdentificacion(clienteDto.getIdentificacion());
                TipoIdentificacion ti = new TipoIdentificacion();
                ti.setTipoIdentificacion(clienteDto.getTipoIdentificacion());
                cliente.setTipoIdentificacion(ti);
                clienteService.save(cliente);
                FotoClienteDto dto = clienteService.actualizarConFoto(cliente, archivo);
                return dto != null ? ResponseEntity.status(HttpStatus.CREATED).body(dto) :
                        ResponseEntity.noContent().build();
            } catch (JsonProcessingException e) {
                logger.severe(e.getMessage());
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
            }
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Elimina el cliente por identificador")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Eliminado"),
            @ApiResponse(code = 404, message = "No se encuentra el cliente")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminar(@PathVariable int id) {
        Optional<Cliente> optCliente = clienteService.findById(id);
        if (optCliente.isPresent()) {
            clienteService.delete(optCliente.get());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("Consulta los clientes por diferentes filtros opcionales aplicando paginación y devuelve un listado de clientes con información de la paginación")
    @ApiResponse(code = 200, message = "Ok", response = Cliente.class, responseContainer = "List")
    @GetMapping("/consultar-por-filtros")
    public ResponseEntity<Page<Cliente>> buscarPorFiltros(@RequestParam(required = false) String nombre, @RequestParam(required = false) String apellido,
                                                          @RequestParam(required = false) Integer tipoId, @RequestParam(required = false) Integer edad,
                                                          @RequestParam(required = false) Integer edadMin, @RequestParam(required = false) Integer edadMax,
                                                          @RequestParam(required = false) String ciudadNac, Pageable paginacion) {
        return ResponseEntity.ok(clienteService.buscarPorFiltros(nombre, apellido, tipoId, edad, edadMin, edadMax, ciudadNac, paginacion));
    }

    @ApiOperation("Consulta los clientes con su foto por diferentes filtros opcionales aplicando paginación y devuelve un listado de clientes con información de la paginación")
    @ApiResponse(code = 200, message = "Ok", response = FotoClienteDto.class, responseContainer = "List")
    @GetMapping("/fotos/consultar-por-filtros")
    public ResponseEntity<Page<FotoClienteDto>> buscarPorFiltrosConFoto(@RequestParam(required = false) String nombre, @RequestParam(required = false) String apellido,
                                                                        @RequestParam(required = false) Integer tipoId, @RequestParam(required = false) Integer edad,
                                                                        @RequestParam(required = false) Integer edadMin, @RequestParam(required = false) Integer edadMax,
                                                                        @RequestParam(required = false) String ciudadNac, Pageable paginacion) {
        return ResponseEntity.ok(clienteService.buscarPorFiltrosConFoto(nombre, apellido, tipoId, edad, edadMin, edadMax, ciudadNac, paginacion));
    }
}
