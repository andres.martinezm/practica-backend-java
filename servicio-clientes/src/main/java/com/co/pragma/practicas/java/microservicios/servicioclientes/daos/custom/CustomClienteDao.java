package com.co.pragma.practicas.java.microservicios.servicioclientes.daos.custom;

import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomClienteDao {
    Page<Cliente> buscarPorFiltros(String nombre, String apellido, String tipoId, String edad, String edadMin, String edadMax,
                                   String ciudadNac, Pageable paginacion);
}
