package com.co.pragma.practicas.java.microservicios.servicioclientes.services;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.co.pragma.practicas.java.microservicios.libs.libcommon.services.MasterService;
import com.co.pragma.practicas.java.microservicios.servicioclientes.daos.ClienteDao;
import com.co.pragma.practicas.java.microservicios.servicioclientes.feign.ArchivosClienteFeign;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.ClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ClienteService extends MasterService<Cliente, Integer, ClienteDao> {

    private ArchivosClienteFeign archivosClienteFeign;

    public ClienteService(ClienteDao repository) {
        super(repository);
    }

    @Transactional(readOnly = true)
    public Page<Cliente> buscarPorFiltros(String nombre, String apellido, Integer tipoId, Integer edad, Integer edadMin,
                                          Integer edadMax, String ciudadNac, Pageable paginacion) {
        return dao.buscarPorFiltros(nombre != null ? nombre : "", apellido != null ? apellido : "", tipoId != null ? tipoId : -1, edad != null ? edad : -1,
                edadMin != null ? edadMin : -1, edadMax != null ? edadMax : -1, ciudadNac != null ? ciudadNac : "", paginacion);
    }

    @Transactional(readOnly = true)
    public Page<FotoClienteDto> buscarPorFiltrosConFoto(String nombre, String apellido, Integer tipoId, Integer edad, Integer edadMin,
                                                        Integer edadMax, String ciudadNac, Pageable paginacion) {
        return dao.buscarPorFiltros(nombre != null ? nombre : "", apellido != null ? apellido : "", tipoId != null ? tipoId : -1, edad != null ? edad : -1,
                edadMin != null ? edadMin : -1, edadMax != null ? edadMax : -1, ciudadNac != null ? ciudadNac : "", paginacion)
                .map(this::mapFotoClienteDtoPorId);
    }

    public FotoClienteDto registrarConFoto(ClienteDto clienteDto, MultipartFile archivo) {
        Cliente cliente = new Cliente(clienteDto.getNombres(), clienteDto.getApellidos(), new TipoIdentificacion(clienteDto.getTipoIdentificacion(), null, null),
                clienteDto.getIdentificacion(), clienteDto.getEdad(), clienteDto.getCiudadNacimiento());
        cliente = save(cliente);
        FotoClienteDto cl = archivosClienteFeign.registrar(cliente.getCliente(), archivo);
        if (cl != null)
            cl.setInfoCliente(cliente);
        else
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return cl;
    }

    @Transactional(readOnly = true)
    public Iterable<FotoClienteDto> consultarTodosConFoto() {
        return ((List<Cliente>) findAll()).stream()
                .map(this::mapFotoClienteDtoPorId)
                .collect(Collectors.toList());
    }

    private FotoClienteDto mapFotoClienteDtoPorId(Cliente c) {
        FotoClienteDto clienteDto;
        try {
            clienteDto = archivosClienteFeign.buscarPorClienteId(c.getCliente());
        } catch (FeignException.NotFound nfe) {
            clienteDto = new FotoClienteDto();
        }
        clienteDto.setCliente(c.getCliente());
        clienteDto.setInfoCliente(c);
        return clienteDto;
    }

    @Transactional(readOnly = true)
    public Page<FotoClienteDto> consultarTodosConFoto(Pageable paginacion) {
        return findAll(paginacion).map(this::mapFotoClienteDtoPorId);
    }

    @Transactional(readOnly = true)
    public Optional<FotoClienteDto> buscarPorIdConFoto(int cliente) {
        return findById(cliente).map(this::mapFotoClienteDtoPorId);
    }

    public FotoClienteDto actualizarConFoto(Cliente cliente, MultipartFile archivo) {
        save(cliente);
        FotoClienteDto cl;
        try {
            cl = archivosClienteFeign.actualizar(cliente.getCliente(), archivo);
        } catch (FeignException.NotFound nfe) {
            cl = archivosClienteFeign.registrar(cliente.getCliente(), archivo);
        }
        if (cl != null)
            cl.setInfoCliente(cliente);
        else
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return cl;
    }

    @Override
    public void delete(Cliente entity) {
        super.delete(entity);
        try {
            archivosClienteFeign.eliminar(entity.getCliente());
        } catch (FeignException.NotFound ignored) {
        }
    }

    @Autowired
    public void setArchivosClienteFeign(ArchivosClienteFeign archivosClienteFeign) {
        this.archivosClienteFeign = archivosClienteFeign;
    }
}
