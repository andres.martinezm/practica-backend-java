package com.co.pragma.practicas.java.microservicios.servicioclientes.feign;

import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "servicio-archivos", path = "/fotos-clientes")
public interface ArchivosClienteFeign {

    @GetMapping("/{id}")
    FotoClienteDto buscarPorId(@PathVariable String id);

    @GetMapping("/cliente/{id}")
    FotoClienteDto buscarPorClienteId(@PathVariable Integer id);

    @DeleteMapping("/{id}")
    void eliminar(@PathVariable String id);

    @DeleteMapping("/cliente/{id}")
    void eliminar(@PathVariable Integer clienteId);

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    FotoClienteDto registrar(@RequestParam Integer clienteId, @RequestPart MultipartFile archivo);

    @PutMapping(path = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    FotoClienteDto actualizar(@PathVariable String id, @RequestPart MultipartFile archivo);

    @PutMapping(path = "/cliente/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    FotoClienteDto actualizar(@PathVariable Integer id, @RequestPart MultipartFile archivo);
}
