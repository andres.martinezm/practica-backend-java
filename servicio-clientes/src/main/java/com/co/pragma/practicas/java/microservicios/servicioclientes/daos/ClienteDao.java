package com.co.pragma.practicas.java.microservicios.servicioclientes.daos;

import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ClienteDao extends PagingAndSortingRepository<Cliente, Integer> {

    @Query("select c  " +
            "from Cliente c  " +
            "where (:nombre = '' or c.nombres like concat(:nombre, '%'))  " +
            "and (:apellido = '' or c.apellidos like concat(:apellido, '%'))  " +
            "and (:tipoId = -1 or c.tipoIdentificacion = :tipoId)  " +
            "and (:edad = -1 or c.edad = :edad)  " +
            "and (:edadMin = -1 and :edadMax = -1 " +
            "  OR (:edadMin != -1 AND :edadMax != -1 AND c.edad BETWEEN :edadMin AND :edadMax)   " +
            "    OR (:edadMin != -1 AND :edadMax = -1 AND c.edad >= :edadMin)  " +
            "    OR (:edadMin = -1 AND :edadMax != -1 AND c.edad <= :edadMax))  " +
            "and (:ciudadNac = '' or c.ciudadNacimiento = :ciudadNac)")
    Page<Cliente> buscarPorFiltros(@Param("nombre") String nombre, @Param("apellido") String apellido, @Param("tipoId") Integer tipoId,
                                   @Param("edad") Integer edad, @Param("edadMin") Integer edadMin, @Param("edadMax") Integer edadMax,
                                   @Param("ciudadNac") String ciudadNac, Pageable paginacion);
}