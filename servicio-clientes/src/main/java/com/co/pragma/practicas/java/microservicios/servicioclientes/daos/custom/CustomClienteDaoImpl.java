package com.co.pragma.practicas.java.microservicios.servicioclientes.daos.custom;

import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class CustomClienteDaoImpl implements CustomClienteDao {

    private final EntityManager em;

    public CustomClienteDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Page<Cliente> buscarPorFiltros(String nombre, String apellido, String tipoId, String edad, String edadMin, String edadMax, String ciudadNac, Pageable paginacion) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cliente> cq = cb.createQuery(Cliente.class);
        Root<Cliente> cliente = cq.from(Cliente.class);


        cq
                .select(cliente)
                .where(cb.like(cliente.get("nombre"), ""));
        return null;
    }
}
