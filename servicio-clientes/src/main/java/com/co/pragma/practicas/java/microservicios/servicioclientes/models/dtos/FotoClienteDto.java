package com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos;

import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;

public class FotoClienteDto {
    private String id;
    private Integer cliente;
    private byte[] foto;
    private Cliente infoCliente;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Cliente getInfoCliente() {
        return infoCliente;
    }

    public void setInfoCliente(Cliente infoCliente) {
        this.infoCliente = infoCliente;
    }
}
