package com.co.pragma.practicas.java.microservicios.servicioclientes;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@EnableFeignClients
@SpringBootApplication
@EntityScan(basePackages = "com.co.pragma.practicas.java.microservicios")
public class ServicioClientesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicioClientesApplication.class, args);
    }

    @Bean
    public Module hibernate5Module() {
        Hibernate5Module mod = new Hibernate5Module();
        mod.enable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);
        return mod;
    }

}
