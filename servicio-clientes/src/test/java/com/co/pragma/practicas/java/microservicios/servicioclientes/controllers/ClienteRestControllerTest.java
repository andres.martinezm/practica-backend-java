package com.co.pragma.practicas.java.microservicios.servicioclientes.controllers;

import com.co.pragma.practicas.java.microservicios.servicioclientes.DatoTest;
import com.co.pragma.practicas.java.microservicios.servicioclientes.feign.ArchivosClienteFeign;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.ClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class ClienteRestControllerTest {

    private static ObjectMapper mapper;
    private final String ENDPOINT_BASE = "/clientes";

    @MockBean
    private ArchivosClienteFeign archivosClienteFeign;

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void beforeAll() {
        mapper = new ObjectMapper();
    }

    @BeforeEach
    void setUp() throws IOException {
        FotoClienteDto fotoClienteDto = DatoTest.fotoCliente1();
        when(archivosClienteFeign.buscarPorClienteId(anyInt())).thenReturn(fotoClienteDto);
        doNothing().when(archivosClienteFeign).eliminar(anyInt());
        when(archivosClienteFeign.registrar(anyInt(), any(MultipartFile.class))).thenReturn(fotoClienteDto);
        when(archivosClienteFeign.actualizar(anyInt(), any(MultipartFile.class))).thenReturn(fotoClienteDto);
    }

    @Test
    void registrar() throws Exception {
        ClienteDto cliente = DatoTest.clienteDto1();
        mvc.perform(post(ENDPOINT_BASE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(cliente)))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.cliente").value(4));
    }

    @Test
    void testRegistrar() throws Exception {
        ClienteDto clienteDto = DatoTest.clienteDto1();
        mvc.perform(multipart(ENDPOINT_BASE)
                .file("archivo", "ABC".getBytes(StandardCharsets.UTF_8))
                .param("clienteJson", mapper.writeValueAsString(clienteDto)))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.infoCliente.cliente").value(5))
                .andExpect(jsonPath("$.foto").isNotEmpty());
    }

    @Test
    void consultarTodos() throws Exception {
        mvc.perform(get(ENDPOINT_BASE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(3))))
                .andExpect(jsonPath("$[?(@.identificacion == '49948383')]", hasSize(1)));
    }

    @Test
    void consultarTodosConFoto() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/fotos")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(3))))
                .andExpect(jsonPath("$[?(@.infoCliente.identificacion == '49948383')].foto").isNotEmpty());
    }

    @Test
    void testConsultarTodos() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/paginar")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(greaterThanOrEqualTo(3))))
                .andExpect(jsonPath("$.totalElements", greaterThanOrEqualTo(3)))
                .andExpect(jsonPath("$.content[?(@.identificacion == '49948383')]", hasSize(1)));
    }

    @Test
    void testConsultarTodosConFoto() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/paginar/fotos")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(greaterThanOrEqualTo(3))))
                .andExpect(jsonPath("$.totalElements", greaterThanOrEqualTo(3)))
                .andExpect(jsonPath("$.content[?(@.infoCliente.identificacion == '49948383')].foto").isNotEmpty());
    }

    @Test
    void buscarPorId() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/3")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apellidos").value("Mtz"));
    }

    @Test
    void buscarPorIdConFoto() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/3/foto")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.foto").isNotEmpty())
                .andExpect(jsonPath("$.infoCliente.identificacion").value("49948383"));

        mvc.perform(get(ENDPOINT_BASE + "/10/foto"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void actualizar() throws Exception {
        ClienteDto clienteDto = DatoTest.clienteDto1();
        mvc.perform(put(ENDPOINT_BASE + "/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(clienteDto)))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(jsonPath("$.cliente").value(1))
                .andExpect(jsonPath("$.identificacion").value("012239957"));
    }

    @Test
    void testActualizar() throws Exception {
        ClienteDto clienteDto = DatoTest.clienteDto1();
        mvc.perform(multipart(ENDPOINT_BASE + "/1")
                .file("archivo", "ABC".getBytes(StandardCharsets.UTF_8))
                .param("clienteJson", mapper.writeValueAsString(clienteDto))
                .with(new RequestPostProcessor() {
                    @Override
                    public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                        request.setMethod(HttpMethod.PUT.name());
                        return request;
                    }
                }))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.infoCliente.cliente").value(1))
                .andExpect(jsonPath("$.foto").isNotEmpty())
                .andExpect(jsonPath("$.infoCliente.identificacion").value("012239957"));
    }

    @Test
    void eliminar() throws Exception {
        mvc.perform(delete(ENDPOINT_BASE + "/2")).andExpect(status().is(HttpStatus.OK.value()));
        mvc.perform(get(ENDPOINT_BASE + "/2")).andExpect(status().is4xxClientError());
    }

    @Test
    void buscarPorFiltros() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/consultar-por-filtros")
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("edadMin", "30"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(greaterThanOrEqualTo(1))));
    }

    @Test
    void buscarPorFiltrosConFoto() throws Exception {
        mvc.perform(get(ENDPOINT_BASE + "/fotos/consultar-por-filtros")
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("edadMin", "30"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(greaterThanOrEqualTo(1))));
    }
}