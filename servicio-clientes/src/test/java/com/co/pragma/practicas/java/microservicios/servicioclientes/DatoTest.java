package com.co.pragma.practicas.java.microservicios.servicioclientes;

import com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities.TipoIdentificacion;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.ClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

public class DatoTest {
    public static Cliente cliente1() {
        Cliente c = new Cliente();
        c.setNombres("Elizabeth");
        c.setApellidos("Taylor Mei");
        c.setEdad(23);
        c.setIdentificacion("102030405");
        c.setCiudadNacimiento("Hashrum");
        TipoIdentificacion ti = new TipoIdentificacion();
        ti.setTipoIdentificacion(1);
        c.setTipoIdentificacion(ti);
        return c;
    }

    public static FotoClienteDto fotoCliente1() throws IOException {
        var resource = new ClassPathResource("imagen1.jpg");
        var fotoClienteDto = new FotoClienteDto();
        fotoClienteDto.setId("abcdefg12345678");
        fotoClienteDto.setFoto(resource.getInputStream().readAllBytes());
        return fotoClienteDto;
    }

    public static ClienteDto clienteDto1() {
        var c = new ClienteDto();
        c.setNombres("Jimena Esther");
        c.setApellidos("Paez De La Rosa");
        c.setEdad(19);
        c.setIdentificacion("012239957");
        c.setCiudadNacimiento("Cali");
        c.setTipoIdentificacion(4);
        return c;
    }
}
