package com.co.pragma.practicas.java.microservicios.servicioclientes.daos;

import com.co.pragma.practicas.java.microservicios.servicioclientes.DatoTest;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ClienteDaoTest {

    @Autowired
    private ClienteDao clienteDao;

    @Test
    void save() {
        Cliente cliente = DatoTest.cliente1();
        cliente = clienteDao.save(cliente);
        assertNotNull(cliente.getCliente());
        assertEquals(4, cliente.getCliente());
    }

    @Test
    void findAll() {
        List<Cliente> clientes = (List<Cliente>) clienteDao.findAll();
        assertFalse(clientes.isEmpty());
    }

    @Test
    void findById() {
        Optional<Cliente> optCliente = clienteDao.findById(2);
        assertTrue(optCliente.isPresent());
        assertEquals("103338839", optCliente.get().getIdentificacion());
        assertEquals(23, optCliente.get().getEdad());
    }

    @Test
    void findAllPage() {
        Page<Cliente> clientes = clienteDao.findAll(PageRequest.of(0, 10));
        assertFalse(clientes.getContent().isEmpty());
    }

    @Test
    void update() {
        Optional<Cliente> optTipoId = clienteDao.findById(2);
        assertTrue(optTipoId.isPresent());
        Cliente c = optTipoId.get();
        c.setIdentificacion("123456");
        c.setEdad(49);
        clienteDao.save(c);
        c = clienteDao.findById(2).orElseThrow();

        assertEquals("123456", c.getIdentificacion());
        assertEquals(49, c.getEdad());
    }

    @Test
    void delete() {
        clienteDao.deleteById(1);
        assertTrue(clienteDao.findById(1).isEmpty());
    }

    @Test
    void buscarPorFiltros() {
        Page<Cliente> clientesP = clienteDao.buscarPorFiltros("", "", -1, -1, 30, -1, "", PageRequest.of(0, 10));
        assertFalse(clientesP.isEmpty());
        assertEquals(2, clientesP.getTotalElements());
    }
}