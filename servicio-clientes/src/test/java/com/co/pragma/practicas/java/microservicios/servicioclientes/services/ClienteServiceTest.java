package com.co.pragma.practicas.java.microservicios.servicioclientes.services;

import com.co.pragma.practicas.java.microservicios.servicioclientes.DatoTest;
import com.co.pragma.practicas.java.microservicios.servicioclientes.feign.ArchivosClienteFeign;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.dtos.FotoClienteDto;
import com.co.pragma.practicas.java.microservicios.servicioclientes.models.entities.Cliente;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class ClienteServiceTest {

    private final Pageable paginacion = PageRequest.of(0, 10);
    @Autowired
    private ClienteService clienteService;
    @MockBean
    private ArchivosClienteFeign archivosClienteFeign;

    @BeforeEach
    void setUp() throws IOException {
        FotoClienteDto fotoClienteDto = DatoTest.fotoCliente1();
        when(archivosClienteFeign.buscarPorClienteId(anyInt())).thenReturn(fotoClienteDto);
        doNothing().when(archivosClienteFeign).eliminar(anyInt());
        when(archivosClienteFeign.registrar(anyInt(), any(MultipartFile.class))).thenReturn(fotoClienteDto);
        when(archivosClienteFeign.actualizar(anyInt(), any(MultipartFile.class))).thenReturn(fotoClienteDto);
    }

    @Test
    void findAll() {
        List<Cliente> clientes = (List<Cliente>) clienteService.findAll();
        assertFalse(clientes.isEmpty());
    }

    @Test
    void testFindAll() {
        Page<Cliente> clientes = clienteService.findAll(paginacion);
        assertFalse(clientes.getContent().isEmpty());
    }

    @Test
    void findById() {
        Optional<Cliente> optCliente = clienteService.findById(2);
        assertTrue(optCliente.isPresent());
        assertEquals("103338839", optCliente.get().getIdentificacion());
        assertEquals(23, optCliente.get().getEdad());
    }

    @Test
    void save() {
        Cliente cliente = DatoTest.cliente1();
        cliente = clienteService.save(cliente);
        assertNotNull(cliente.getCliente());
        assertEquals(4, cliente.getCliente());
    }

    @Test
    void delete() {
        Optional<Cliente> optCliente = clienteService.findById(1);
        clienteService.delete(optCliente.orElseThrow());
        assertTrue(clienteService.findById(1).isEmpty());
    }

    @Test
    void registrarConFoto() {
        FotoClienteDto fotoClienteDto = clienteService.registrarConFoto(DatoTest.clienteDto1(), new MockMultipartFile("name", new byte[0]));
        assertNotNull(fotoClienteDto);
        assertNotNull(fotoClienteDto.getInfoCliente());
        assertEquals(5, fotoClienteDto.getInfoCliente().getCliente());
    }

    @Test
    void consultarTodosConFoto() {
        List<FotoClienteDto> clientes = (List<FotoClienteDto>) clienteService.consultarTodosConFoto();
        assertFalse(clientes.isEmpty());
        assertTrue(clientes.stream().allMatch(c -> c.getId() != null && c.getInfoCliente() != null));
    }

    @Test
    void buscarPorIdConFoto() {
        Optional<FotoClienteDto> fotoClienteDto = clienteService.buscarPorIdConFoto(3);
        assertTrue(fotoClienteDto.isPresent());
        assertNotNull(fotoClienteDto.get().getFoto());
        assertTrue(fotoClienteDto.get().getFoto().length > 0);
        assertNotNull(fotoClienteDto.get().getInfoCliente());
    }

    @Test
    void actualizarConFoto() {
        Optional<Cliente> optTipoId = clienteService.findById(2);
        assertTrue(optTipoId.isPresent());
        Cliente c = optTipoId.get();
        c.setIdentificacion("123456");
        c.setEdad(49);
        clienteService.actualizarConFoto(c, new MockMultipartFile("name", new byte[0]));
        c = clienteService.findById(2).orElseThrow();

        assertEquals("123456", c.getIdentificacion());
        assertEquals(49, c.getEdad());
    }

    @Test
    void testDelete() {
        Optional<Cliente> optTipoId = clienteService.findById(2);
        assertTrue(optTipoId.isPresent());
        clienteService.delete(optTipoId.get());

        assertTrue(clienteService.findById(2).isEmpty());
    }

    @Test
    void buscarPorFiltros() {
        Page<Cliente> clientes = clienteService.buscarPorFiltros(null, null, null, null, 30, null, null, paginacion);
        assertFalse(clientes.isEmpty());
        assertEquals(1, clientes.getTotalElements());
    }

    @Test
    void buscarPorFiltrosConFoto() {
        Page<FotoClienteDto> clientes = clienteService.buscarPorFiltrosConFoto(null, null, null, null, 30, null, null, paginacion);
        assertFalse(clientes.isEmpty());
        assertTrue(clientes.stream().allMatch(c -> c.getId() != null && c.getInfoCliente() != null));

    }

    @Test
    void testConsultarTodosConFoto() {
        Page<FotoClienteDto> clientes = clienteService.consultarTodosConFoto(paginacion);
        assertFalse(clientes.isEmpty());
        assertTrue(clientes.stream().allMatch(c -> c.getId() != null && c.getInfoCliente() != null));
    }
}