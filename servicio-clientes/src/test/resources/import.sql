INSERT INTO tipos_identificacion (tipo_identificacion, descripcion, codificacion) VALUES(1, 'CEDULA DE CIUDADANIA', 'CC');
INSERT INTO tipos_identificacion (tipo_identificacion, descripcion, codificacion) VALUES(2, 'TARJETA DE IDENTIDAD', 'TI');
INSERT INTO tipos_identificacion (tipo_identificacion, descripcion, codificacion) VALUES(3, 'CEDULA DE EXTRANJERIA', 'CE');
INSERT INTO tipos_identificacion (tipo_identificacion, descripcion, codificacion) VALUES(4, 'PASAPORTE', 'PP');
INSERT INTO clientes (nombres, apellidos, tipo_identificacion, identificacion, edad, ciudad_nacimiento) VALUES('Valeria', 'Vega Méndez', 3, '1122334455', 30, 'Miami');
INSERT INTO clientes (nombres, apellidos, tipo_identificacion, identificacion, edad, ciudad_nacimiento) VALUES('Andrés Fel', 'Mtz', 1, '103338839', 23, 'Barranquilla');
INSERT INTO clientes (nombres, apellidos, tipo_identificacion, identificacion, edad, ciudad_nacimiento) VALUES('Beto', 'Mtz', 1, '49948383', 31, 'Soledad');