package com.co.pragma.practicas.java.microservicios.libs.libcommon.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.Optional;

public class MasterService<E, I extends Serializable, R extends CrudRepository<E, I>> {

    protected final R dao;

    public MasterService(R repository) {
        dao = repository;
    }

    public Iterable<E> findAll() {
        return dao.findAll();
    }

    @SuppressWarnings("unchecked")
    public Page<E> findAll(Pageable page) {
        if (dao instanceof PagingAndSortingRepository)
            return ((PagingAndSortingRepository<E, I>) dao).findAll(page);
        throw new UnsupportedOperationException();
    }

    public Optional<E> findById(I id) {
        return dao.findById(id);
    }

    public E save(E entity) {
        return dao.save(entity);
    }

    public void delete(E entity) {
        dao.delete(entity);
    }
}
