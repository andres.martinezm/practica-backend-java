package com.co.pragma.practicas.java.microservicios.libs.libcommon.models.entities;

import javax.persistence.*;

@Table(name = "tipos_identificacion")
@Entity
public class TipoIdentificacion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tipo_identificacion", nullable = false)
    private Integer tipoIdentificacion;

    @Column(name = "descripcion", length = 100)
    private String descripcion;

    @Column(name = "codificacion", length = 8)
    private String codificacion;

    public TipoIdentificacion() {
    }

    public TipoIdentificacion(Integer tipoIdentificacion, String descripcion, String codificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.descripcion = descripcion;
        this.codificacion = codificacion;
    }

    public String getCodificacion() {
        return codificacion;
    }

    public void setCodificacion(String codificacion) {
        this.codificacion = codificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Integer tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
}