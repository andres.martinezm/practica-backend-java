package com.co.pragma.practicas.java.microservicios.zuulgateway.config;

import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.InMemorySwaggerResourcesProvider;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Configuration
public class SwaggerDocumentationConfig {

    private final ZuulProperties zuulProperties;

    public SwaggerDocumentationConfig(ZuulProperties zuulProperties) {
        this.zuulProperties = zuulProperties;
    }

    @Primary
    @Bean
    public SwaggerResourcesProvider swaggerResourcesProvider(InMemorySwaggerResourcesProvider defaultResourcesProvider) {
        return () -> {
            Map<String, ZuulProperties.ZuulRoute> routes = zuulProperties.getRoutes();
            List<SwaggerResource> resources = new ArrayList<>();
            routes.forEach((k, v) -> resources.add(createSwResource(v.getServiceId(), String.format("%sv2/api-docs",
                    zuulProperties.getPrefix() + v.getPath().replace("**", "")), "1.0")));
            return resources;
        };
    }

    private SwaggerResource createSwResource(String name, String location, String version) {
        var r = new SwaggerResource();
        r.setName(name);
        r.setLocation(location);
        r.setSwaggerVersion(version);
        return r;
    }
}
